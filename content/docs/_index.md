---
title: Documentation
lastmod: 2023-09-01T17:47:22.842Z
---

## Download TGDP Templates as Archetypes

All the template files in [The Good Docs Project (TGDP) Templates](https://gitlab.com/tgdp/templates) **main** branch can be added to the Hugo archetypes directory through the following command.

```
wget -c https://gitlab.com/tgdp/templates/-/archive/main/templates-main.tar.gz \
&& tar -zxvf templates-main.tar.gz --transform='s:.*/::' -C archetypes --no-anchored --wildcards '*.md' \
&& find archetypes -type f -not -name "template*.md" -delete \
&& for file in ./archetypes/template-*.md; do sed -i -e '1 e cat archetypes/templates-frontmatter.md' "$file"; done \
&& rm templates-main.tar.gz
```

**NOTE:** *The command above will not work for all the 'template' files in the **v1.0.0** release. This is due to filename inconsistencies that [have been fixed](https://gitlab.com/tgdp/templates/-/merge_requests/301) since that release.*

A specific release version of the templates are added by slightly changing the command above.

Replace `/main/templates-main.tar.gz` in the GitLab URL with the release version path like `/v1.0.0/templates-v1.0.0.tar.gz`.

Then, update the end of the command to remove (rm) the correct .tar.gz filename, `rm templates-v1.0.0.tar.gz` becomes `rm templates-main.tar.gz`.

NOTE: This command above will not work for all the 'template' files in the v1.0.0 release, due to some filename inconsistencies that were fixed after the v1 release.

## Create New Content from Template Archetype

Content can be created from the [The Good Docs Project Templates](https://gitlab.com/tgdp/templates) using the following command format.

```
hugo new docs/<filename-that-becomes-the-title>.md -k template-<name>
```

For example to use the **tutorial** template to create a post with the filename/slug of `this-is-how-you-do-something` entitled "This is how you do something" the command would be.

```
hugo new docs/this-is-how-you-do-something.md -k template-tutorial
```

## Learn more about each Template

The Good Docs Project Templates for Documentation content types, have their own documentation too!

If you visit the [The Good Docs Project Templates](https://gitlab.com/tgdp/templates) project on GitLab, you will see folders for each template in the repository.
Browsing into one of the folders will typically provide you with two files.  

* `guide-<template-name>.md` - The Guide that explains the template and how to use it.
* `tutorial-<template-name>.md` - The Template file itself that we extract into the archetypes folder in the command above.

These guides contain practical information along with the wisdom of those who come before us.

If you see anything missing or that needs to be fixed in a template or the associated guide, please find the 'feedback form' link at the bottom of the template file and submit your comments to the project for that specific template.
