---
title: Hello World
date: 2022-02-04T21:48:46.000Z
draft: false
layout: page
page_sections:
  - background_choice: gray
    fieldGroup: hero
    heading_markdown: Hello **World**
  - label: Content Section
    fieldGroup: content
    contentString: This is some **Markdown** content in a block 'Param' that uses a
      shortcode. {{< world "Front Matter" >}}
  - heading_markdown: Another Heading
    fieldGroup: hero
    background_choice: black
    text_choice: white
  - title: Kitchen Sink
    fieldGroup: all
    description: |-
      This is testing all the field types, with **Markdown** formatting of
      course.  

      Including paragraph breaks and all the bells and whistles.  

      - How about a bullet list.

      - Does that do anything for you?
    date: 2022-02-14T18:21:07.492Z
    toggled: true
    background_choice: gray
    text_choice: white
    photo:
      caption: Pretty Kitty
      image: /uploads/241831987_10227025478486818_5435483714951671474_n.jpg
    feeling:
      - Cheerful
      - Enthralled
      - Optimistic
      - Content
    authors:
      - name: Bryan Klein
        fieldGroup: author
        social:
          - name: Twitter
            fieldGroup: social
          - name: LinkedIn
            fieldGroup: social
      - name: Carrie Klein
        fieldGroup: author
        social:
          - name: LinkedIn
            fieldGroup: social
type: page
lastmod: 2022-07-09T01:48:48.582Z
description: Stuff here.
preview: /uploads/241831987_10227025478486818_5435483714951671474_n.jpg
---

## Heading 1

Some additional content for the `.Content` variable in a **block**.

> This is a blockquote in markdown.
